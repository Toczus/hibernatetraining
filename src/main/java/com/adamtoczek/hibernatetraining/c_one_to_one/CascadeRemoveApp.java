package com.adamtoczek.hibernatetraining.c_one_to_one;

import com.adamtoczek.hibernatetraining.c_one_to_one.entity.Company;
import com.adamtoczek.hibernatetraining.c_one_to_one.entity.CompanyDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class CascadeRemoveApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();






        session.beginTransaction();

        Company company = session.get(Company.class, 33);

        session.remove(company);


        session.getTransaction().commit();




        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
