package com.adamtoczek.hibernatetraining.c_one_to_one;

import com.adamtoczek.hibernatetraining.c_one_to_one.entity.Company;
import com.adamtoczek.hibernatetraining.c_one_to_one.entity.CompanyDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class OneToOneHqlApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        String select = "select c from Company c";
        String where = "select c from Company c join c.companyDetail cd where cd.residence='Italy'";
        String sum = "select sum(c.value) from Company c join c.companyDetail cd where cd.residence='Poland'";
        String orderBy = "select c.name from CompanyDetail cd join cd.company c where cd.employeeNumber < 35000 order by c.value";

        session.beginTransaction();

        Query query = session.createQuery(orderBy);

//		List<Company> resultList = query.getResultList();
//		Long result = (Long)query.getSingleResult();
        List<String> resultList = query.getResultList();

        session.getTransaction().commit();

//		for(Company c : resultList) {
//			System.out.println(c + ", " + c.getCompanyDetail());
//		}

//		System.out.println("wartosc wszystkich polskich firm w bazie danych: " + result);

        for(String c : resultList) {
            System.out.println(c);
        }

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
