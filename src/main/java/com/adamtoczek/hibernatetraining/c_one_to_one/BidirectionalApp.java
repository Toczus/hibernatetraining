package com.adamtoczek.hibernatetraining.c_one_to_one;

import com.adamtoczek.hibernatetraining.c_one_to_one.entity.Company;
import com.adamtoczek.hibernatetraining.c_one_to_one.entity.CompanyDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class BidirectionalApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();



//		Company company = new Company("PZU", 1000000);
//		CompanyDetail detail = new CompanyDetail("Poland",17000);
//		detail.setCompany(company);
//		company.setCompanyDetail(detail);

        session.beginTransaction();

//		session.persist(detail);
        CompanyDetail detail = session.get(CompanyDetail.class, 23);
        session.remove(detail);

        session.getTransaction().commit();


//		System.out.println(detail.getCompany());



        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
