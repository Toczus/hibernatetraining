package com.adamtoczek.hibernatetraining.a_basics;

import com.adamtoczek.hibernatetraining.a_basics.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class PrimaryKeyApp {

    public static void main(String[] args) {

        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(Employee.class);
        SessionFactory factory = configuration.buildSessionFactory();
        Session session = factory.getCurrentSession();

        Employee employee1 = new Employee();
        employee1.setFirstName("Jan");
        employee1.setLastName("Kowalski");
        employee1.setSalary(2500);

        Employee employee2 = new Employee();
        employee2.setFirstName("Jan");
        employee2.setLastName("Nowak");
        employee2.setSalary(2800);

        Employee employee3 = new Employee();
        employee3.setFirstName("Jerzy");
        employee3.setLastName("Owsiak");
        employee3.setSalary(3200);

        session.beginTransaction();
        session.save(employee1);
        session.save(employee2);
        session.save(employee3);
        session.getTransaction().commit();
        factory.close();
    }
}
