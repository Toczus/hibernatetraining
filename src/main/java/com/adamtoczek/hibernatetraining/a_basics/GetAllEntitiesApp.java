package com.adamtoczek.hibernatetraining.a_basics;

import com.adamtoczek.hibernatetraining.a_basics.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class GetAllEntitiesApp {

    public static void main(String[] args) {


        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(Employee.class);

        SessionFactory factory = configuration.buildSessionFactory();
        Session session = factory.getCurrentSession();

        // Rozpoczęcie transakcji
        session.beginTransaction();

        List<Employee> resultList = session.createQuery("from Employee").getResultList();

        for (Employee employee : resultList) {
            System.out.println(employee);
        }


        // Zakończenie transakcji
        session.getTransaction().commit();
        factory.close();
    }
}
