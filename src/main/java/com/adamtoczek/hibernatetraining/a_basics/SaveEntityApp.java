package com.adamtoczek.hibernatetraining.a_basics;

import com.adamtoczek.hibernatetraining.a_basics.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class SaveEntityApp {

    public static void main(String[] args) {

        // Stworzenie obiektu Configuration
        Configuration conf = new Configuration();

        // Wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");

        // Wczytanie adnotacji klasy Employee
        conf.addAnnotatedClass(Employee.class);

        // Stworzneie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();

        // Pobranie sesji
        Session session =  factory.getCurrentSession();

        // Stworzenie obiektu klasy Employee
        Employee employee = new Employee();
        employee.setFirstName("Jan");
        employee.setLastName("Kowalski");
        employee.setSalary(3200);

        // Rozpoczęcie transakcji
        session.beginTransaction();

        // Zapisanie pracownika
        session.save(employee);

        // Zakończenie transakcji
        session.getTransaction().commit();

        // Zamknięcie obiektu SessionFactory
        factory.close();
    }
}
