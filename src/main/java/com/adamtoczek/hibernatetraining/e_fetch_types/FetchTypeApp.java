package com.adamtoczek.hibernatetraining.e_fetch_types;

import com.adamtoczek.hibernatetraining.e_fetch_types.entity.Company;
import com.adamtoczek.hibernatetraining.e_fetch_types.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.e_fetch_types.entity.Property;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class FetchTypeApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        conf.addAnnotatedClass(Property.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();


        int id = 87;

        session.beginTransaction();

        System.out.println("Pobieranie obiektu company");
        Company company = session.get(Company.class, id);
        System.out.println("Pobrano obiekt company");
        System.out.println(company);

        System.out.println("Nieruchomosci:");
        for(Property property : company.getProperties()) {
            System.out.println(property);
        }



        session.getTransaction().commit();



        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
