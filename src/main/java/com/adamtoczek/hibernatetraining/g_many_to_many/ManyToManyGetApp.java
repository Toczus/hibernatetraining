package com.adamtoczek.hibernatetraining.g_many_to_many;

import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Company;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Department;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Employee;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Property;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Training;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 09.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class ManyToManyGetApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        conf.addAnnotatedClass(Property.class);
        conf.addAnnotatedClass(Department.class);
        conf.addAnnotatedClass(Employee.class);
        conf.addAnnotatedClass(Training.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        int id = 4;

        session.beginTransaction();

//				Training training = new Training("java training");
//
//				Employee employee1 = session.get(Employee.class, 303);
//				Employee employee2 = session.get(Employee.class, 304);
//
//				training.addEmployee(employee1);
//				training.addEmployee(employee2);
//
//				session.persist(training);

        String getTraining = "from Training";

        Query query = session.createQuery(getTraining);

        List<Training> resultList = query.getResultList();

        for(Training training : resultList) {
            System.out.println("\n" + training);
            for(Employee employee : training.getEmployees()) {
                System.out.println("- " + employee);
            }
        }

//				Training training = session.get(Training.class, id);
//
//
//				System.out.println(training);
//
//				for(Employee employee : training.getEmployees()) {
//					System.out.println("- " + employee);
//				}



        session.getTransaction().commit();

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
