package com.adamtoczek.hibernatetraining.g_many_to_many;

import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Company;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Department;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Employee;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Property;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Training;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 09.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class ManyToManySaveApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        conf.addAnnotatedClass(Property.class);
        conf.addAnnotatedClass(Department.class);
        conf.addAnnotatedClass(Employee.class);
        conf.addAnnotatedClass(Training.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();


        session.beginTransaction();

        Training training = new Training("sales training");

        Employee employee = new Employee("Johny","Depp",16000);
        Employee employee2 = new Employee("Miley","Cyrus",16000);

        training.addEmployee(employee);
        training.addEmployee(employee2);

        session.persist(training);

        session.getTransaction().commit();

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
