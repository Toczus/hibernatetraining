package com.adamtoczek.hibernatetraining.g_many_to_many;

import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Company;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Department;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Employee;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Property;
import com.adamtoczek.hibernatetraining.g_many_to_many.entity.Training;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 09.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class ManyToManyDeleteApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        conf.addAnnotatedClass(Property.class);
        conf.addAnnotatedClass(Department.class);
        conf.addAnnotatedClass(Employee.class);
        conf.addAnnotatedClass(Training.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();


        int idEmployee = 304;
        int idTraining = 4;

        session.beginTransaction();

//		Employee employee = session.get(Employee.class, idEmployee);
//
//		session.delete(employee);

        Training training = session.get(Training.class, idTraining);

        session.delete(training);


        session.getTransaction().commit();

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
