package com.adamtoczek.hibernatetraining.d_one_to_many;

import com.adamtoczek.hibernatetraining.d_one_to_many.entity.Company;
import com.adamtoczek.hibernatetraining.d_one_to_many.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.d_one_to_many.entity.Property;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class OneToManyDeleteApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company .class);
        conf.addAnnotatedClass(CompanyDetail .class);
        conf.addAnnotatedClass(Property .class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        String getCompany = "select c from Company c where c.name='Strefakursow'";

        session.beginTransaction();
        Query query = session.createQuery(getCompany);
//		Company company = (Company) query.getSingleResult();
//
//
//		for(Property property : company.getProperties()) {
//			if("Gdynia".equals(property.getCity())) {
//				session.delete(property);
//			}
//		}

        int idToDelete = 6;

        Property property = session.get(Property.class, idToDelete);

        session.delete(property);


        session.getTransaction().commit();

        // zamknięcie obiektu SessionFactory
        factory.close();
    }

}
