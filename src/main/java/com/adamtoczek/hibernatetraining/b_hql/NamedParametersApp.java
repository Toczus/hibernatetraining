package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.b_hql.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class NamedParametersApp {

    public static void main(String[] args) {


        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Employee.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        String employeeFirstName = "Steven";
        String employeeLastName = "King";

        session.beginTransaction();

        // Konkatenacja stringów jest nieelegancka, poza tym istnieje podatność na SqlInjection
        String queryString = "select e.firstName, e.lastName, e.salary from Employee e where e.firstName='"
                + employeeFirstName + "' and e.lastName='" + employeeLastName + "'";

        Query query = session.createQuery(queryString);

        // Zapytanie wykorzystujące NamedParameters
        String namedParametersString = "select e.firstName, e.lastName, e.salary from Employee e where e.firstName=:firstName and e.lastName=:lastName";

        Query namedParametersQuery = session.createQuery(namedParametersString);
        // Ustawiamy parametry z zapytania
        namedParametersQuery.setParameter("firstName", employeeFirstName);
        namedParametersQuery.setParameter("lastName", employeeLastName);

        List<Object[]> result = namedParametersQuery.getResultList();

        session.getTransaction().commit();

        for (Object[] values : result) {
            System.out.println("firstName: " + values[0] + ", lastName: " + values[1] + ", salary: " + values[2]);
        }

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
