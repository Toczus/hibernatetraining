package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.b_hql.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class UpdateApp {

    public static void main(String[] args) {


        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Employee.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();


        int idEmployee = 297;
        int salary = 16000;

        session.beginTransaction();

        String update = "update Employee e set e.salary=:salary where e.idEmployee=:idEmployee";
        Query query = session.createQuery(update);
        query.setParameter("salary", salary);
        query.setParameter("idEmployee", idEmployee);
        query.executeUpdate();




        session.getTransaction().commit();


        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
