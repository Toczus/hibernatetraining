package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.b_hql.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class OrderByApp {

    public static void main(String[] args) {


        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Employee.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        session.beginTransaction();

        // sortujemy po imieniu a-z
        String orderBy = "select e.firstName, e.lastName from Employee e order by e.firstName";
        // sortujemy po nazwisku z-a
        String orderBy2 = "select e.firstName, e.lastName from Employee e order by e.lastName desc";
        // sortujemy po wypłacie 0 ->
        String orderBy3 = "select e.firstName, e.lastName, e.salary from Employee e order by e.salary asc";

        Query query = session.createQuery(orderBy3);
        List<Object[]> result = query.getResultList();





        session.getTransaction().commit();

        for(Object[] values : result) {
            System.out.println("firstName: " + values[0] + ", lastName: " + values[1] + ", salary: " + values[2]);
        }


        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
