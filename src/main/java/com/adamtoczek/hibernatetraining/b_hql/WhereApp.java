package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.b_hql.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class WhereApp {

    public static void main(String[] args) {


        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Employee.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        session.beginTransaction();

        String where = "from Employee where firstName='Tadeusz'";
        String where2 = "from Employee where salary > 12000";
        String where3 = "from Employee where salary < 3000 or salary > 13000";
        String where4 = "from Employee where salary is null";
        String where5 = "from Employee where lastName in ('Hutton','Errazuriz','Wiśniewski')";

        Query query = session.createQuery(where5);

        List<Employee> list = query.getResultList();

        session.getTransaction().commit();

        for(Employee employee : list) {
            System.out.println(employee);
        }

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
