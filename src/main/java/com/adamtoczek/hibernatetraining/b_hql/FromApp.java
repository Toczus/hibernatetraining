package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.a_basics.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class FromApp {

    public static void main(String[] args) {

        // Stworzenie obiektu Configuration
        Configuration conf = new Configuration();

        // Wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");

        // Wczytanie adnotacji klasy Employee
        conf.addAnnotatedClass(Employee.class);

        // Stworzneie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();

        // Pobranie sesji
        Session session = factory.getCurrentSession();

        // Rozpoczęcie transakcji
        session.beginTransaction();

        String from = "FROM Employee";
        String from2 = "from Employee"; // można pisać małymi
        String from3 = "from com.adamtoczek.hibernatetraining.hql.entity.Employee"; // można napisać całą ścieżkę do
        // pakietu

        Query query = session.createQuery(from);

        List<Employee> list = query.getResultList();

        // Zakończenie transakcji
        session.getTransaction().commit();

        for (Employee employee : list) {
            System.out.println(employee);
        }

        // Zamknięcie obiektu SessionFactory
        factory.close();


    }
}
