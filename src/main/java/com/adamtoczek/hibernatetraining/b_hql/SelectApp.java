package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.b_hql.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class SelectApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Employee.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        session.beginTransaction();

        String select = "select firstName, lastName from Employee";             // Piszemy nazwy pól naszej encji
        String select2 = "select e.firstName, e.lastName from Employee as e";   // Korzystamy z aliasów
        String select3 = "select e.firstName, e.lastName from Employee e";      // Słowo "as" jest opcjonalne

        Query query = session.createQuery(select3);
        List<Object[]> result = query.getResultList();  // Tablica obiektów




        session.getTransaction().commit();

        for(Object[] values : result) {
            System.out.println("firstName: " + values[0] + ", lastName: " + values[1]);
        }


        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
