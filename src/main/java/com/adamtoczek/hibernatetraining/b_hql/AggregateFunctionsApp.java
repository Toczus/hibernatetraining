package com.adamtoczek.hibernatetraining.b_hql;

import com.adamtoczek.hibernatetraining.b_hql.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 * Created by Adam Toczek on 07.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class AggregateFunctionsApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Employee.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        session.beginTransaction();

        // średnia wartość wypłaty
        String avg = "select avg(e.salary) from Employee e";
        // suma wszystkich wypłat
        String sum = "select sum(e.salary) from Employee e";
        // minimalna wypłata
        String min = "select min(e.salary) from Employee e";
        // maksymalna wypłata
        String max = "select max(e.salary) from Employee e";
        // Zwraca liczbę wystąpień właściwości czyli w tym przypadku ilość rekordów z wypłatą
        String count = "select count(e.salary) from Employee e";

        Query query = session.createQuery(count);


        // Należy zwracać uwagę na typ zwracany wyniku aby nie dostawać błędu
        Long result = (Long) query.getSingleResult();




        session.getTransaction().commit();

        System.out.println("wynik: " + result);


        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
