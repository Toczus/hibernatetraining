package com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional;

import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.Company;
import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.Department;
import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.Property;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class OneToManyUniDeleteApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        conf.addAnnotatedClass(Property.class);
        conf.addAnnotatedClass(Department.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        int id = 7;
        int idCompany = 92;
        String departmentNameToDelete = "HR";
        int idHql = 9;

        String delete = "delete Department d where d.idDepartment=:idDepartment";

        session.beginTransaction();

//				Department department = session.get(Department.class, id);
//				session.delete(department);

//				Company company = session.get(Company.class, idCompany);
//
//				for(Department department : company.getDepartments()) {
//					if(department.getName().equals(departmentNameToDelete)) {
//						company.getDepartments().remove(department);
//						session.delete(department);
//					}
//				}

        Query query = session.createQuery(delete);
        query.setParameter("idDepartment", idHql);
        int deletedRows = query.executeUpdate();

        System.out.println("ilosc usunietych rekordow: " + deletedRows);


        session.getTransaction().commit();

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
