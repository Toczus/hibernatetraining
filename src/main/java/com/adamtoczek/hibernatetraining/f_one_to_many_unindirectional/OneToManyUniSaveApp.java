package com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional;

import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.Company;
import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.CompanyDetail;
import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.Department;
import com.adamtoczek.hibernatetraining.f_one_to_many_unindirectional.entity.Property;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by Adam Toczek on 08.12.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class OneToManyUniSaveApp {

    public static void main(String[] args) {

        // stworzenie obiektu Configuration
        Configuration conf = new Configuration();
        // wczytanie pliku konfiguracyjnego
        conf.configure("hibernate.cfg.xml");
        // wczytanie adnotacji
        conf.addAnnotatedClass(Company.class);
        conf.addAnnotatedClass(CompanyDetail.class);
        conf.addAnnotatedClass(Property.class);
        conf.addAnnotatedClass(Department.class);
        // stworzenie obiektu SessionFactory
        SessionFactory factory = conf.buildSessionFactory();
        // pobranie sesji
        Session session = factory.getCurrentSession();

        int id = 92;

        session.beginTransaction();
        Company company = session.get(Company.class, id);
        System.out.println(company);

        Department department1 = new Department("sales");
        Department department2 = new Department("production");
        Department department3 = new Department("HR");

        company.addDepartment(department1);
        company.addDepartment(department2);
        company.addDepartment(department3);

        session.persist(company);

        session.getTransaction().commit();

        // zamknięcie obiektu SessionFactory
        factory.close();
    }
}
