USE hibernate_training;

DROP TABLE IF EXISTS training;

CREATE TABLE training (
	id_training int(11) NOT NULL AUTO_INCREMENT,
    name varchar(128) DEFAULT NULL,
    PRIMARY KEY (id_training)
) ENGINE=InnoDB  DEFAULT CHARSET= utf8mb4;

DROP TABLE IF EXISTS employee_training;

CREATE TABLE employee_training (
  id_employee int(11) NOT NULL,
  id_training int(11) NOT NULL,
  
  PRIMARY KEY (id_employee,id_training),
  
  KEY `FK_TRAINING_idx` (id_training),
  
  CONSTRAINT `FK_EMPLOYEE_05` FOREIGN KEY (id_employee) 
  REFERENCES employee (id_employee) 
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  CONSTRAINT `FK_TRAINING` FOREIGN KEY (id_training) 
  REFERENCES training (id_training) 
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET= utf8mb4;
