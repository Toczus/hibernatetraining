USE hibernate_training;

DROP TABLE IF EXISTS property;

CREATE TABLE property (
	id_property int(11) NOT NULL AUTO_INCREMENT,
    city varchar(128) DEFAULT NULL,
    room_number int(11) DEFAULT NULL,
    id_company int(11) DEFAULT NULL,
    PRIMARY KEY (id_property),
    CONSTRAINT FK_COMPANY FOREIGN KEY (id_company) REFERENCES company (id_company)
) ENGINE=InnoDB  DEFAULT CHARSET= utf8mb4;
