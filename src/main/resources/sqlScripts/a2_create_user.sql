CREATE USER 'adam'@'localhost' IDENTIFIED WITH mysql_native_password BY 'adam';

GRANT ALL PRIVILEGES ON * . * TO 'adam'@'localhost';