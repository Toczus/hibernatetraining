USE hibernate_training;


DROP TABLE IF EXISTS company_detail;

CREATE TABLE company_detail (
	id_company_detail int(11) NOT NULL AUTO_INCREMENT,
	residence varchar(256) DEFAULT NULL,
	employee_number int(11) DEFAULT NULL,
	PRIMARY KEY (id_company_detail)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET= utf8mb4;

DROP TABLE IF EXISTS company;

CREATE TABLE company (
	id_company int(11) NOT NULL AUTO_INCREMENT,
	name varchar(256) DEFAULT NULL,
	value int(11) DEFAULT NULL,
	id_company_detail int(11) DEFAULT NULL,
	PRIMARY KEY (id_company),
	CONSTRAINT FK_COMPANY_DETAIL FOREIGN KEY (id_company_detail) REFERENCES company_detail (id_company_detail)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET= utf8mb4;
