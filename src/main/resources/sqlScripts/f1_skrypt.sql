USE hibernate_training;

DROP TABLE IF EXISTS department;

CREATE TABLE department (
	id_department int(11) NOT NULL AUTO_INCREMENT,
    name varchar(128) DEFAULT NULL,
    id_company int(11) DEFAULT NULL,
    PRIMARY KEY (id_department),
    CONSTRAINT FK_COMPANY_DIVISION FOREIGN KEY (id_company) REFERENCES company (id_company)
) ENGINE=InnoDB  DEFAULT CHARSET= utf8mb4;